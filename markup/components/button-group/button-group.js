
//active
$('.button-group').each(function (i) {
    $(this).find(".button-group__item").click(function () {
        $(this).parent().find(".button-group__item").removeClass('active');
        $(this).addClass('active');
        return false;
    });
});
//active

$(".tabs-level .button-group__item").click(function () {
    $('.gameboard__panel').fadeIn();
    var level = $(this).attr('data-level');
    $('.gameboard__icon').removeClass('gameboard__icon_active');
    var CloneElement = $('.gameboard__item:first').clone();
    $('.gameboard__item').addClass('animate-out');
    setTimeout(function () {
        $('.gameboard__item').removeClass('animate-out');
    }, 200);
    setTimeout(function () {
        var countElement = $('.gameboard__item').length
        $('.gameboard__item').removeClass('gameboard__item_rotated');
        $('.gameboard__balloon ').removeClass('gameboard__balloon_red gameboard__balloon_blue gameboard__balloon_green gameboard__balloon_luck')


        for (i = countElement; i > level; i--) {
            $('.gameboard__item:first').remove();
        }
        for (i = countElement; i < level; i++) {
            var countElement = $('.gameboard__item').length
            var CloneElement = $('.gameboard__item:first').clone();
            $('.gameboard__items').append(CloneElement);
        }
    }, 150);
    setTimeout(function () {
        var countElement = $('.gameboard__item').length;
        $('.gameboard__item').removeClass('disable no-active grid-block');
        if (countElement == 16) {
            $('.gameboard__items').addClass('big');
            $('.gameboard__items').removeClass('small');

        }
        if (countElement == 9) {
            $('.gameboard__items').removeClass('big');
            $('.gameboard__items').removeClass('small');
        }
        if (countElement == 6) {
            $('.gameboard__item:last-child').addClass('disable no-active grid-block')
            $('.gameboard__items').addClass('small');
            $('.gameboard__items').removeClass('big');
        }
        

    }, 150);
    return false;
});
$(".tabs-level .button-group__item").click(function () {
    var level = $(this).attr('data-level');
    var CloneElement = $('.gameboard__board-panel__level-item:first').clone();
    setTimeout(function () {
        var countElement = $('.gameboard__board-panel__level-item').length
    
        for (i = countElement; i > level; i--) {
            $('.gameboard__board-panel__level-item:first').remove();
        }
        for (i = countElement; i < level; i++) {
            var countElement = $('.gameboard__board-panel__level-item').length
            var CloneElement = $('.gameboard__board-panel__level-item:first').clone();
            $('.gameboard__board-panel__level-items').append(CloneElement);
            $('.gameboard__board-panel__level-item').removeClass('active');

        }
    }, 150);
    setTimeout(function () {
        var countElement = $('.gameboard__board-panel__level-item').length
        if (countElement == 6) {
            $('.gameboard__board-panel__level-item:last-child').remove();
            $('.gameboard__board-panel__level-items').addClass('small');
        }
    }, 150);
    setTimeout(function () {
        $('.gameboard__board-panel__level-item:last-child').addClass('active');
    }, 170);
    return false;
});
