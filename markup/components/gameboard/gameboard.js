
var z_index = 2000;

$('.gameboard__icon').each(function(i){
    $(this).addClass('gameboard__icon_disabled')
});
$('body').on('click', '.gameboard__icon:not(.gameboard__icon_disabled)', function (e) {

    var random_number_for_img = random_integer(1, 3);
    if (random_number_for_img == 1) {
        $(this).closest('.gameboard__item')
            .find('.gameboard__balloon').addClass('gameboard__balloon_blue');
    }
    else if (random_number_for_img == 2) {
        $(this).closest('.gameboard__item')
            .find('.gameboard__balloon').addClass('gameboard__balloon_red');
    }
    else {
        $(this).closest('.gameboard__item')
            .find('.gameboard__balloon').addClass('gameboard__balloon_green');
    }
    var target = this;
    var is_gamestart = $(this).closest('.gameboard__item_hover').length;
    if (is_gamestart) {
        $(this).closest('.gameboard__item_hover').fadeOut();
        $(this).closest('.gameboard__items').find('.gameboard__item').addClass('gameboard__item_disabled');
        $(this).closest('.gameboard__item').find('.gameboard__icon svg').css('opacity', 1);
    }
    $(this).closest('.gameboard__items').find('.gameboard__icon').addClass('gameboard__icon_disabled');
    $(this).closest('.gameboard__item').addClass('gameboard__item_waiting');
    z_index++;
    setTimeout(function () {
        $('#panel-game').find('.button_theme_lg').hide();
        $('.gameboard__button_finish').show();
        $(target).closest('.gameboard__item')
            .find('.gameboard__balloon')
            .addClass('gameboard__balloon_luck');
        $(target).closest('.gameboard__item')
            .addClass('gameboard__item_rotated')
            .css('z-index', z_index);
        $(target).closest('.gameboard__items')
            .find('.gameboard__item:not(.gameboard__item_rotated) .gameboard__icon')
            .removeClass('gameboard__icon_disabled');
        if (is_gamestart) {
            $(target).closest('.gameboard__items').find('.gameboard__item').removeClass('gameboard__item_disabled');
        }

        var is_gamefinish = !$('.gameboard__item:not(.gameboard__item_rotated)').length;

        setTimeout(function () {
            $(target).closest('.gameboard__item')
                .removeClass('gameboard__item_waiting');
        }, 800);

        if (is_gamefinish) {
            setTimeout(function () {
                $('.gameboard__item:nth-child(4):not(.gameboard__item_rotated) .gameboard__balloon, .gameboard__item:nth-child(5):not(.gameboard__item_rotated) .gameboard__balloon, .gameboard__item:nth-child(6):not(.gameboard__item_rotated) .gameboard__balloon').addClass('gameboard__balloon_transform_invert');
                $('.gameboard__item:nth-child(4):not(.gameboard__item_rotated), .gameboard__item:nth-child(5):not(.gameboard__item_rotated), .gameboard__item:nth-child(6):not(.gameboard__item_rotated)').addClass('gameboard__item_transform_invert');
                $('.gameboard__balloon.gameboard__balloon_transform_invert').css('opacity', 1);
                $('.gameboard__item').each(function () {
                    $(this)
                        .removeClass('gameboard__item_rotated')
                        .removeClass('gameboard__item_delay')
                        .removeClass('gameboard__item_transform_invert');
                    $(this).find('.gameboard__balloon').removeClass('gameboard__balloon_transform_invert');
                    $(this).find('.gameboard__icon').removeClass('gameboard__icon_disabled');
                });
            }, 4000);
        }


    }, 1000);
});

//functions
function random_integer(min, max) {
    var rand = min + Math.random() * (max - min)
    rand = Math.round(rand);
    return rand;
}

$("#summaPlus").keyup(function () {
    var $this = $(this);
    var maxLen = 3;
    if ($this.val() > 200) {
        $('.gameboard__countg').text($("#summaPlus").val(200));
    }
    else {
        $('.gameboard__countg').text($("#summaPlus").val());
    }
    var w1 = 10;
    var w2 = 1;
    var w3 = 50;
    var w4 = 100;

    if ($this.val() == w1) {
        $('.gameboard__rate-item').removeClass('gameboard__rate-item_active');
        $('.gameboard__rate-item[data-value="' + w1 + '"').addClass('gameboard__rate-item_active');

    }
    if ($this.val() == w2) {
        $('.gameboard__rate-item').removeClass('gameboard__rate-item_active');
        $('.gameboard__rate-item[data-value="' + w2 + '"').addClass('gameboard__rate-item_active');

    }
    if ($this.val() == w3) {
        $('.gameboard__rate-item').removeClass('gameboard__rate-item_active');
        $('.gameboard__rate-item[data-value="' + w3 + '"').addClass('gameboard__rate-item_active');

    }
    if ($this.val() == w4) {
        $('.gameboard__rate-item').removeClass('gameboard__rate-item_active');
        $('.gameboard__rate-item[data-value="' + w4 + '"').addClass('gameboard__rate-item_active');

    }

    if ($this.val().length > maxLen) {
        $this.val($this.val().substr(0, maxLen));
    }
    $('.gameboard__countg').text($("#summaPlus").val());
    $('.gameboard__rate .price').text($("#summaPlus").val());

});
$('body').on('click', '.gameboard__rate-item', function (e) {
    $(this).closest('.gameboard__rate-items')
        .find('.gameboard__rate-item')
        .removeClass('gameboard__rate-item_active');
    $(this).addClass('gameboard__rate-item_active');
    var countg = $(this).html();
    $(this).closest('.gameboard').find('.gameboard__countg').html(countg);
    $('#summaPlus').val(countg);
    $('.gameboard__rate .price').text($("#summaPlus").val());

});
$('body').on('click', '#start', function (e) {
    $('.gameboard__icon').addClass('gameboard__icon_active');
    $('.gameboard__icon').removeClass('gameboard__icon_disabled');
    $('.gameboard__error').slideDown();
    $('.gameboard').addClass('game-start');
});


var isMobile = false;
// проверка на размер экрана (размер я брал вроде с Bootstrap-а)
$(document).ready(function () {
    if ($('body').width() <= 719) {
        isMobile = true;
    }
    // и потом если нужен код только для телефона:
    if (isMobile) {
        $('body').on('click', '#start', function (e) {
            $('.gameboard__panel').fadeOut();
        });
        $('.gameboard__items').each(function () {
            var height = $(this).height();
            $('.gameboard__panel ').css('height', height);
        });
        
        $(window).resize(function () {
            $('.gameboard__items').each(function () {
                var height = $(this).height();
                $('.gameboard__panel ').css('height', height);
            });
        });
    }
    // или для остальных
    if (!isMobile) {
        $('.advantages_slider_true').trigger('destroy.owl.carousel');
    }
});





$('body').on('click', '.gameboard__button_finishgame', function (e) {
    var target = this;
    $(this).attr('disabled', 'disabled');
    $('.gameboard__item:nth-child(4):not(.gameboard__item_rotated) .gameboard__balloon, .gameboard__item:nth-child(5):not(.gameboard__item_rotated) .gameboard__balloon, .gameboard__item:nth-child(6):not(.gameboard__item_rotated) .gameboard__balloon').addClass('gameboard__balloon_transform_invert');
    $('.gameboard__item:nth-child(4):not(.gameboard__item_rotated), .gameboard__item:nth-child(5):not(.gameboard__item_rotated), .gameboard__item:nth-child(6):not(.gameboard__item_rotated)').addClass('gameboard__item_transform_invert');
    setTimeout(function () {
        var z_index = 2000;
        $('.gameboard__items')
            .find('.gameboard__item')
            .each(function () {
                if (!$(this).hasClass('gameboard__item_rotated')) {
                    var random_number_for_img = random_integer(1, 3);
                    if (random_number_for_img == 1) {
                        $(this).closest('.gameboard__item')
                            .find('.gameboard__balloon').addClass('gameboard__balloon_blue');
                    }
                    else if (random_number_for_img == 2) {
                        $(this).closest('.gameboard__item')
                            .find('.gameboard__balloon').addClass('gameboard__balloon_red');
                    }
                    else {
                        $(this).closest('.gameboard__item')
                            .find('.gameboard__balloon').addClass('gameboard__balloon_green');
                    }
                    var that = this;
                    z_index--;
                    $(this)
                        .addClass('gameboard__item_rotated');
                    $(this).css('z-index', z_index);
                    setTimeout(function () {
                        $(that).closest('.gameboard__item');
                        $('.gameboard__balloon.gameboard__balloon_transform_invert').css('opacity', 1);
                        setTimeout(function () {
                            $('#panel-game').find('.button_theme_lg').show();
                            $('.gameboard__button_finish').hide();
                            $('.gameboard__item').each(function () {
                               $(this)
                                    .removeClass('gameboard__item_rotated')
                                    .removeClass('gameboard__item_delay')
                                    .removeClass('gameboard__icon_active')
                                    .removeClass('gameboard__item_transform_invert')
                               $(this).find('.gameboard__balloon').removeClass('gameboard__balloon_transform_invert');
                               $(this).find('.gameboard__icon').removeClass('gameboard__icon_disabled');
                            });
                            $(target).removeAttr('disabled');
                        }, 2000);
                    }, 800);
                }
        });
    }, 1000);
});