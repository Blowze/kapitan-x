$(".drop-link").click(function () {
    var dropContent = $(this).attr('href');
    $(dropContent).slideToggle(300);
    if (!$(this).data('status')) {
        $(this).html('Скрыть');
        $(this).data('status', true);
    }
    else {
        $(this).html('Как играть');
        $(this).data('status', false);
    }
    return false;

});

