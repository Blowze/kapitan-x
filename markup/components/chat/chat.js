
$('.tabs__item').on('click', function (e) {
    $(".chat-messages-desktop")[0].scrollTo(0, $(".chat-messages-desktop")[0].scrollHeight)
    $('.tabs__item').removeClass('active');
    $('.live').removeClass('slideInDown');
    $(this).addClass('active');
    e.preventDefault();
          var index = $(this).closest('.tabs__item').index();
          $('.tabs__content .tabs__content-item').fadeOut();
          $('.tabs__content .tabs__content-item').eq(index).fadeIn();
      });
  $('.chat-open').on('click', function (e) {
    $('body').toggleClass('open-chat-page');
    $('.chat').toggleClass('open');
    $(this).toggleClass('open');
  })
  $('.chat-icon').on('click', function (e) {
    $('body').toggleClass('open-chat-page');
    $('.chat').toggleClass('open');
    $('.chat-open').toggleClass('open');

  })
 
  $(document).ready(function(){
       $(".chat-messages-desktop")[0].scrollTo(0, $(".chat-messages-desktop")[0].scrollHeight)
  })
  $(document).on("click",".chat__messages-item.answer a",function(){var e=$("#message");if(-1!==e.val().indexOf("@"))return!1;var s=$(this).attr("name").replace("m","@");e.val(s+" "+e.val()).focus()})
  
    $('.chat__footer a').click(function(){
      
        var chat = $('.chat__main');
        var chatItems = $('.chat__messages').length;
        var value = $('.chat__footer input').val();
        var message = $('.chat__messages').first().clone().hide().slideDown().fadeIn(200);
        chat.animate({scrollTop: (chat.prop('scrollHeight') * chatItems)});
      
      if(value != ''){
  
        $(message).find('.chat__messages-text').html(value);
        $('<div class="chat__messages">\
              <div class="chat__messages-name">\
                <span>Адам Адамович</span>\
              </div>\
              <div class="chat__messages-text">'+ value + '</div>\
              </div>\
          </div>').hide().appendTo(".chat__main").slideDown(200).fadeIn(200);
        $('.chat__footer input').val('');
        $('.chat__footer input').removeClass('required');
      } else {
        $('.chat-error').addClass('required');
      }
      return false;
    });
  function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
  function addLive(){
    var randomText = getRandomInRange(1, 1000000);
    var live = $('.live-main .live').first().clone();
    $(live).find('.live__main-price span').html(getRandomInRange(1, 1000000));
    $('.live-main').prepend(live).find('.live').first().slideUp(0).slideDown(400).fadeIn(400)
  }
  setInterval(function(){
      addLive();
      $(".live__main-price span").each(function () {
      var text = $(this).text();
      $(this).html(divideNumberByPieces(text));
    });
    
  
  }, 5000); // 1000 м.сек
  setInterval(function(){
    $('.live').removeClass('slideInDown');
  }, 4500); // 1000 м.сек

    setInterval(function(){
    $('.chat-error').removeClass('required');
  }, 5000); 
  
  
  function divideNumberByPieces(x, delimiter) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter || " ");
  }
  $(".live__main-price span").each(function () {
    var text = $(this).text();
    $(this).html(divideNumberByPieces(text));
  });
    $('.drop-menu li').click(function() {
      var liveLink = $(this).attr('data-live');
      $('.block-live__slider').owlCarousel('destroy');
      $('.block-live__slider').removeClass('owl-carousel');

      setTimeout(function () {
        $('.block-live__slider').addClass('owl-carousel');
        $('.block-live__slider').owlCarousel({
          loop:true,
          autoWidth: true,
          items:1,
          center: true,
          margin:10,
      });
    },100)
      var actives = $(this).text();
      console.log(liveLink);
      $('.live').hide().removeClass('active categories');
      $(liveLink).show().addClass('active categories');
      
      $('.live__footer span:not(.arrow)').text(actives);
       $('.drop-menu li').removeClass('active categories');
      $(this).addClass('active categories');
      
   });
    $('.dropdown').click(function() {
       $('.drop-menu').toggleClass('active');
      $(this).toggleClass('active');
   });
  