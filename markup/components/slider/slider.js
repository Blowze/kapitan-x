
var isMobile = false;
// проверка на размер экрана (размер я брал вроде с Bootstrap-а)
$(document).ready( function() {
    if ($('body').width() <= 719) {
        isMobile = true;
    }
    // и потом если нужен код только для телефона:
    if (isMobile) {
        $('.block-live__slider').addClass('owl-carousel');
        $('.advantages_slider_true').addClass('owl-carousel');
        $('.button-group_slider_yes').addClass('owl-carousel');
        setTimeout(function() { 
            $('.block-live__slider').owlCarousel({
                loop:true,
                autoWidth: true,
                items:1,
                center: true,
                margin:10,
            });
            $('.advantages_slider_true').owlCarousel({
                loop:true,
                autoWidth: true,
                items:1,
                center: true,
                margin:10,
    
            });
            $('.button-group_slider_yes').owlCarousel({
                loop:false,
                nav: true,
                dots: false,
                items:1,
                center: true,
                margin:0,
    
            });
        }, 1000);
        
       
    }
    // или для остальных
    if (!isMobile) {
        $('.advantages_slider_true').trigger('destroy.owl.carousel');
    }
} );
$('.button-group_slider_yes').on('changed.owl.carousel', function(e) {
    const $firstActive = $(this).find('.owl-item.active:eq(1)');   
    var levelMob = $firstActive.find( ".button-group__item").attr('data-level');
});

